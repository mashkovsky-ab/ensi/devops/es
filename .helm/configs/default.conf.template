proxy_cache_path /var/lib/nginx/proxy_cache levels=1:2 keys_zone=all:64m max_size=${CACHE_SIZE};

server {
    listen 80;
    server_name ${NGINX_DOMAIN};
    root /var/www/data;

    resolver ${RESOLVER} ipv6=off;

    index index.php;

    charset utf-8;

    client_max_body_size 500M;
    large_client_header_buffers 4 16k;
    client_body_buffer_size 32K;
    client_header_buffer_size 2k;
    http2_max_header_size 512k;
    http2_max_field_size 256k;
    disable_symlinks off;

    location /favicon.ico {
        access_log off;
    }

    location / {
        expires max;
        add_header Cache-Control "public";
    }
}

server {
    listen 80;
    server_name ${IMGPROXY_DOMAIN};
    root /var/www/data;

    resolver ${RESOLVER} ipv6=off;

    index index.php;

    charset utf-8;

    client_max_body_size 500M;
    large_client_header_buffers 4 16k;
    client_body_buffer_size 32K;
    client_header_buffer_size 2k;
    http2_max_header_size 512k;
    http2_max_field_size 256k;
    disable_symlinks off;

    location /favicon.ico {
        access_log off;
    }

    location / {
        set $upstream ${IMGPROXY_SERVICE_HOST};
        expires  max;
        add_header Cache-Control "public";
        proxy_pass http://$upstream;

        proxy_cache all;
        proxy_cache_valid 404 502 503 1m;
        proxy_cache_valid any 12h;
        proxy_cache_key $uri;
    }
}