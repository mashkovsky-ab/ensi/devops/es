<?php

$key = $argv[1] ?? null;
$salt = $argv[2] ?? null;
$url = $argv[3] ?? null;

$width = $argv[4] ?? 300;
$height = $argv[5] ?? 300;


if (!$key || !$salt || !$url) {
    die('Usage: createUrl key salt path');
}

$keyBin = pack("H*" , $key);
if(empty($keyBin)) {
    die('Key expected to be hex-encoded string');
}

$saltBin = pack("H*" , $salt);
if(empty($saltBin)) {
    die('Salt expected to be hex-encoded string');
}

$resize = 'fill';
$gravity = 'no';
$enlarge = 1;
$extension = 'png';

$encodedUrl = rtrim(strtr(base64_encode($url), '+/', '-_'), '=');

$path = "/{$resize}/{$width}/{$height}/{$gravity}/{$enlarge}/{$encodedUrl}.{$extension}";

$signature = rtrim(strtr(base64_encode(hash_hmac('sha256', $saltBin.$path, $keyBin, true)), '+/', '-_'), '=');

print(sprintf("/%s%s\n", $signature, $path));
